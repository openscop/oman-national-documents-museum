#!/bin/sh
echo =====================
echo == Install E2MS software ==
echo =====================

echo Delete existing folder
cd /home/e2ms
rm -fr /home/e2ms/APPe2ms
echo Starting the install from internet...
mkdir /home/e2ms/APPe2ms
cd /home/e2ms/APPe2ms
git clone --depth=1 https://bitbucket.org/openscop/oman-national-documents-museum.git .
ln -s /home/e2ms/MEDIA /home/e2ms/APPe2ms/MEDIA
chmod -R 777 /home/e2ms/APPe2ms
echo Done
