var debug = true;


$(document).ready(function () {

    if (debug) {
        $("body, a, a:hover").css("cursor", "pointer");
    }

    $("#info").html("This computer is " + hostname);
    //$("#"+hostname).hide();

    $("#shutdownAll").click(function (event) {
        event.preventDefault();
        for (i = 101; i <= 107; i++) {
            $.ajax({
                dataType: "html",
                url: "http://192.168.230." + i + ":1234/shutdown.py",
                success: function (data) {
                    $("#info").html(data);
                    alert(data);
                },
                timeout: 2000,
                error: function (xhr, status) {
                    $("#info").html(status);
                }
            });

        }
        return false;
    });


    $(".urlcmd").click(function (event) {
        event.preventDefault();
        console.log('Command : ' + this.href);
        $("#info").html('Your request is being processed...');

        $.ajax({
            dataType: "html",
            url: this.href,
            success: function (data) {
                $("#info").html(data);
                alert(data);
            },
            timeout: 2000,
            error: function (xhr, status) {
                $("#info").html(status);
            }
        });

        return false;
    });


});