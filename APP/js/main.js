var debug = false;
var timeOut = 2; // minutes

var archives;
var scrollStepColumn = 600;
var previusX = 0;

// Mémorise les menus parcourus
var menuParent = new Array;
menuParent[0] = "home";
menuParent[1] = "";
menuParent[2] = "";
menuParent[3] = "";
var menuLevel = 0;


if (debug) {
    $("body, a, a:hover").css("cursor", "pointer");
}


function monter_ecran() {
    scrollPosition = $('html, body').scrollTop();
    $('html').scrollTop(scrollPosition + 806);
}
function descendre_ecran() {
    scrollPosition = $('html, body').scrollTop();
    $('html').scrollTop(scrollPosition - 806);
}

function enableTooltip() {
    $("div.ui-helper-hidden-accessible > div").remove();
    $("div.ui-tooltip").remove();

    $(document).tooltip({
        content: function () {
            return $(this).attr("title");
        },
        position: {
            my: "right-20 bottom"
        },
        track: true,
        show: {
            effect: "fadeIn",
            delay: 1000,
            duration: 1000
        },
        hide: {
            effect: "fadeOut",
            duration: 300
        }
    });
}

function removeLoupe() {
    // suppression des anciennes instances de la loupe
    $(".ok-listener").remove();
    //$("#ok-loupe").remove();
    $("div[id='ok-loupe']").remove();
}

/*
Cette fonction gère l'affichage du menu et appele le 2e écran.
Elle est appelé depuis le 1er écran.
 */
function chargeTheme(event, element) {
    event.preventDefault();

    // si nous sommes dans le cas d'un lien interne au menu
    if ($(element).attr("href") == "#") {

        // récupềre et parcours toutes les classes du lien
        $.each($(element).attr('class').split(' '), function (index, value) {

            // si une classe commence par "go-"
            if (value.substring(0, 3) == "go-") {

                // on mémorise le menu actuel
                x = $(element).parent().data("level");
                menuParent[x] = $(element).parent().attr("id");
                menuLevel = x + 1;

                // on met à jour le fil d'ariane
                // on récupère le texte
                txtEN = $(element).find('span.EN').html();
                txtAR = $(element).find('span.AR').html();
                // on remplace les sauts de lignes
                if (txtEN.search('<br>') != -1) {
                    txtEN = txtEN.substr(txtEN.search('<br>')+4);
                }
                if (txtAR.search('<br>') != -1) {
                    txtAR = txtAR.substr(txtAR.search('<br>')+4);
                }
                $("#ariane span.EN.level" + x).html(txtEN);
                $("#ariane span.AR.level" + x).html(txtAR);
                // on affiche l'élément du fil d'ariane
                $("#ariane .start").show('fade');
                $("#ariane span.EN.level" + x).show('fade');
                $("#ariane span.AR.level" + x).show('fade');
                // on cache les éléments du fil d'ariane obselète
                for (i = x+1; i <= 3; i++) {
                    $("#ariane span.EN.level" + i).hide('fade');
                    $("#ariane span.AR.level" + i).hide('fade');
                }


                // on cache tous les menus
                $("#theme .line").hide(400);

                // on récupère le nom de la div de la ligne à afficher
                id = "#" + value.substring(6, value.length);

                // on la positionne
                initTop = $(id).data('top'); // position initiale
                $(id).css('top', initTop);

                // on l'affiche
                $(id).delay(400).show("slide", {direction: "up"}, 500);

                // si on est dans un sous menu, alors on affiche le bouton de retour au menu parent
                if (menuLevel > 0) {
                    $("#btBackParent").show("slide", {direction: "up"}, 500);
                } else {
                    $("#btBackParent").hide();
                }

            }
        });

    } else {
        // si nous sommes dans le cas d'un appel à la liste des documents d'un thème
        var href = $(element).attr('href');

        // on nettoie
        H1 = false;
        $('#liste H1.path').empty();
        $('#liste .column').remove();
        $('<div class="column"></div>').insertAfter($('#liste H1.content'));
        $("#scrollUp").hide();
        $("#scrollDown").hide();

        // on parcours les archives et on construit la liste
        for (archive in archives) {
            if (archive.substring(0, archive.length-3) == href) {
                if (!H1) {
                    $('#liste H1.path').append(archives[archive]['path']);
                    H1 = true;
                }
                content = '<bt href="#" class="bt go-' + archive + '"><span class="AR">';
                content += archives[archive]['titre']['ar'] + '</span><br><span>'
                content += archives[archive]['titre']['en'] + '</span></bt>'
                $('#liste .column').append(content);
            }
        }

        // on active la barre de défilement améliorée
        $("#liste .column").mCustomScrollbar({
            theme:"rounded-dots",
            callbacks:{
                onOverflowY:function(){
                    $("#scrollUp").show();
                },
                onTotalScroll:function(){
                    $("#scrollUp").hide();
                },
                onTotalScrollBack:function(){
                    $("#scrollDown").hide();
                }
            }
        });

        // on charge les évènements des clicks

        $("#liste .bt").click(function () {
            chargeArchive(this);
        });

        $("#btTop").click(function () {
            descendre_ecran();
        });

        $("#scrollDown").click( function() {
            $('#liste .column').mCustomScrollbar("scrollTo","+=" + scrollStepColumn);
            $("#scrollUp").show();
        });

        $("#scrollUp").click( function() {
            $('#liste .column').mCustomScrollbar("scrollTo","-=" + scrollStepColumn);
            $("#scrollDown").show();
        });

        // on monte l'écran pour afficher la 2e partie
        monter_ecran();
    }

    // on ajoute la class de mise en évidence du bouton
    //$(element).addClass("bt-active");
    return false;
}

/*
Cette fonction charge les archives.
Elle est appelée depuis le 2e écran.
 */
function chargeArchive(element) {
    admin_click=0; // Compte les clics lors de la séquence secrète pour ouvrir le menu d'administration
    admin_click_valide=0; // Compte les clics lors de la séquence secrète pour ouvrir le menu d'administration

    removeLoupe();

    // récuềre et parcours toutes les classes du lien
    $.each($(element).attr('class').split(' '), function (index, value) {

        // si une classe commence par "go-"
        if (value.substring(0, 3) == "go-") {

            // on récupère le nom de la div de la liste à afficher
            go = value.substring(3, value.length);
            console.log("go : " + go);

            // on charge le template archive
            $('#archive').load('archive.html', function () {

                // on charge les descriptions
                $('#archive h1').html(archives[go]['path']);
                $('#archive div.description.AR').hide(0).delay(400).html(archives[go]['description']['ar']).show("slide", {direction: "up"}, 800, function () {
                    $('#archive div.description.EN').hide(0).delay(400).html(archives[go]['description']['en']).show("slide", {direction: "down"}, 800);
                });
                if (debug) {
                    $('#archive #debug').html(go);
                }

                // on affiche le pager
                if (archives[go]['img'].length > 1) {
                    pager = '<div class="onePage-miniBox active hover">';
                    pager += '<img src="' + archives[go]['img'][0] + '_tmb.png">';
                    pager += '</div>';
                    for (i = 1; i < archives[go]['img'].length; i++) {
                        pager += '<div class="onePage-miniBox hover">';
                        pager += '<img src="' + archives[go]['img'][i] + '_tmb.png">';
                        pager += '</div>';
                    }
                    if (archives[go]['img'].length > 8) {
                        $("#pager-scroll").show("slide", {direction: "left"}, 800);
                    } else {
                        $("#pager-scroll").hide();
                    }
                    $('#archive #pager').html(pager).promise().done(function () {

                        // prépare les évènements sur chaque bouton du pager
                        $('#archive #pager img').click(function (event) {
                            // chargement de l'image
                            $('#archive #viewer img').attr('src', $(this).attr('src').substring(0, $(this).attr('src').length - 8));
                            admin_click = 0;
                            return false;
                        });
                    });
                }

                // on indique aux boutons précédents et suivant l'archive vers lesquels ils doivent pointer (class go-...)
                $('#btPreviousStep').addClass('go-' + archives[go]['previous']);
                $('#btNextStep').addClass('go-' + archives[go]['next']);

                // on charge les évènements des clicks
                $("#pager-scroll").click(function () {
                    if ($("#pager").scrollTop() == 0) {
                        $('#pager').animate({
                            scrollTop: 83
                        }, 'slow');
                    }
                    else if ($("#pager").scrollTop() == 83) {
                        if (archives[go]['img'].length > 16) {
                            $('#pager').animate({
                                scrollTop: 166
                            }, 'slow');
                        } else {
                            $('#pager').animate({
                                scrollTop: 0
                            }, 'slow');
                        }
                    }
                    else {
                        $('#pager').animate({
                            scrollTop: 0
                        }, 'slow');
                    }
                });

                $("#btTopBis").click(function () {
                    removeLoupe();
                    descendre_ecran();
                });

                $("#btPreviousStep").click(function () {
                    chargeArchive(this);
                });
                $("#btNextStep").click(function () {
                    chargeArchive(this);
                });
                $(".btBackHome").click(function () {
                    location.reload();
                });

                // on charge la 1ère image
                $('#archive #viewer img').attr('src', archives[go]['img'][0]);

                // on affiche l'archive en se décallant sur le 3e écran
                monter_ecran();

                // séquence secrète pour accéder au menu d'administration
                // accessible depuis l'archive Man-02
                if (go == "Man-02") {
                    // on capte les clics sur la description anglaise
                    $('#archive div.description.EN').click(function () {
                        admin_click++;
                        if (admin_click == 2) {
                            // au 2e clic, on capte les clics sur la description arabe
                            $('#archive div.description.AR').click(function () {
                                admin_click_valide++;
                                // au 2e clic
                                if (admin_click_valide == 2) {
                                    // on charge la page d'admnistration
                                    window.location = "admin.html";

                                }
                            })
                        } else {
                            $('#archive div.description.AR').unbind('click');
                        }
                    })
                } else {
                    // sinon, on s'assure que l'on ne capte par les clics
                    $('#archive div.description.EN').unbind('click');
                    $('#archive div.description.AR').unbind('click');
                }

                // puis après 3 secondes
                setTimeout(
                    function () {
                        // on active la loupe
                        $('#viewer img').okzoom({
                            width: 400,
                            height: 400,
                            background: "#000000",
                            scaleWidth: 1500
                        });

                        // on active les bulles d'aide
                        enableTooltip();

                    }, 1500);

                //$("#liste .bt").removeClass("bt-active");
                //$("#liste .bt.go-" + go).addClass("bt-active");
            });
        }
    });
}


$(document).ready(function () {
    // désactive le menu contextuel
    document.oncontextmenu = function () {
        return false
    };

    //bof detecting inactivity
    idleTime = 0;
    function timerIncrement() {
        idleTime = idleTime + 1;
        if (idleTime > (timeOut -1)) { // 2 minutes
             window.location.reload();
        }
    }
    //Increment the idle time counter every minute.
    var idleInterval = setInterval(timerIncrement, 60000); // 1 minute
    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {
        idleTime = 0;
    });
    $(this).keypress(function (e) {
        idleTime = 0;
    });
    //eof detecting inactivity

    // chargement du fichier des archives (format Json)
    $.getJSON('archives.json', function (data) {
        archives = data;
        $('html, body').scrollTop(0);

        // charge le menu des thèmes
        $('#theme').load('theme.html', function() {
            $('#theme-title img').show("slide", {direction: "up"}, 500, function () {
                $('#theme-title h1').show("slide", {direction: "up"}, 500, function () {
                    $('#theme .X0').css('transition','none');
                    $('#theme .X0').show("slide", {direction: "down"}, 1000, function () {
                        $('#theme .X0').css('transition','top 0.5s ease-in-out');
                        $('#theme-choose div').show("slide", {direction: "up"}, 100);
                    });
                });
            });

            enableTooltip();

            $("#btBackParent").click(function (event) {
                $("#theme .line").hide(400);
                $("#" + menuParent[menuLevel - 1]).delay(400).show("slide", {direction: "up"}, 500);
                menuLevel = menuLevel - 1 ;

                // si on est dans un sous menu, alors on affiche le bouton de retour au menu parent
                if (menuLevel > 0) {
                    $("#btBackParent").show("slide", {direction: "up"}, 500);
                } else {
                    $("#btBackParent").hide();
                }

                // on cache les éléments du fil d'ariane obselète
                for (i = menuLevel; i <= 3; i++) {
                    $("#ariane span.EN.level" + i).hide('fade');
                    $("#ariane span.AR.level" + i).hide('fade');
                }


            });
            $("#theme .bt").click(function (event) {
                chargeTheme(event, this);
            });
            $(".btBackHome").click(function () {
                location.reload();
            });
        });

        
    });
});