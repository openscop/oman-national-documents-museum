#!/bin/sh
# E2MS by Openscop
# ast update : 1/09/2016

echo main.sh
echo E2MS by Openscop

xfce4-terminal -x /home/e2ms/APPe2ms/coutdown.sh

# Disable power manager
echo Disable power manager...
xset s off -dpms

# Lauch E2MS server for remote control
echo Start control_server.py...
/home/e2ms/APPe2ms/control_server.sh > /home/e2ms/APPe2ms/control_server_sh.log &

# Launch firefox with E2MS content
echo Start firefox...
/home/e2ms/APPe2ms/firefox.sh > /home/e2ms/APPe2ms/firefox_sh.log &

sleep 2

# Lauch Kinect
echo Start Kinect... 
/home/e2ms/APPe2ms/Kinect.sh > /home/e2ms/APPe2ms/Kinect_sh.log &

