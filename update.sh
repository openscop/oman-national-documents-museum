#!/bin/sh
echo ==========================
echo == Update E2MS software ==
echo ==      20/09/2016      ==
echo ==========================

echo Backup current software...
cd /home/e2ms
rm -fr /home/e2ms/APPe2ms-bak
mv /home/e2ms/APPe2ms /home/e2ms/APPe2ms-bak
echo Starting the update from internet...
mkdir /home/e2ms/APPe2ms
cd /home/e2ms/APPe2ms
git clone --depth=1 https://bitbucket.org/openscop/oman-national-documents-museum.git .
ln -s /home/e2ms/MEDIA /home/e2ms/APPe2ms/MEDIA
chmod -R 777 /home/e2ms/APPe2ms
echo Done
echo Please, check application and reboot computer...