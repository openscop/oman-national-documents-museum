#!/usr/bin/python
#coding=utf8

date_update = "15/09/2016"

#--------------------------------------------------------------
# This section is defined by installer
httpPort = 1234
#--------------------------------------------------------------

import socket
import os
import sys
import subprocess
from BaseHTTPServer import HTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler

def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 0))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

# The HTTP server
class HTTPHandler (SimpleHTTPRequestHandler):
    server_version = "MonServeurHTTP/0.1"

    def do_GET(self):
        if self.path.find('.py') != -1:
            self.reponse()
        else:
            SimpleHTTPRequestHandler.do_GET(self)

    def reponse(self):
        self.send_response(200, 'OK')
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        htmlReturn = httpRequest(self.path)
        self.wfile.write(htmlReturn)

# Treatment of HTTP request
def httpRequest(httpReq):
    if httpReq !="":
        if httpReq.find('?') != -1:
            page, arg = httpReq.split('?', 1)
            arg = arg.split("&")
            arg_dict = {}
            for i in range(0, len(arg)):
                x, y = arg[i].split("=")
                arg_dict[x] = y
            arg = arg_dict
        else:
            page = httpReq
            arg = ''
        page = page[1:-3]

        httpReturn = False
        if page == "restart":
            print "A remote restart request has arrived."
            httpReturn = "Restart in progress..."
            os.system("sudo shutdown -r now")
            httpReturn += "Done !"

        elif page == "shutdown":
            print "A remote shutdown request has arrived."
            httpReturn = "Shutdown in progress..."
            os.system("sudo shutdown -h now")
            httpReturn += "Done !"

        elif page == "startfirefox":
            print "A start firefox request has arrived."
            httpReturn = "in progress..."
            os.system("pkill -f firefox")
            os.system("/home/e2ms/APPe2ms/firefox.sh > /home/e2ms/APPe2ms/firefox_sh.log &")
            httpReturn += "Done !"

        elif page == "stopfirefox":
            print "A stop firefox request has arrived."
            httpReturn = "in progress..."
            print "I stop firefox..."
            os.system("pkill -f firefox")
            httpReturn += "Done !"

        elif page == "startkinect":
            print "A start kinect request has arrived."
            httpReturn = "in progress..."
            os.system("sudo pkill -f Kinect")
            os.system("/home/e2ms/APPe2ms/Kinect.sh > /home/e2ms/APPe2ms/Kinect_sh.log &")
            httpReturn += "Done !"

        elif page == "stopkinect":
            print "A stop kinect request has arrived."
            httpReturn = "in progress..."
            os.system("sudo pkill -f Kinect")
            httpReturn += "Done !"

        elif page == "startapp":
            print "A restart application request has arrived."
            httpReturn = "in progress..."
            os.system("sudo pkill -f Kinect")
            os.system("pkill -f firefox")
            os.system("/home/e2ms/APPe2ms/firefox.sh > /home/e2ms/APPe2ms/firefox_sh.log &")
            os.system("/home/e2ms/APPe2ms/Kinect.sh > /home/e2ms/APPe2ms/Kinect_sh.log &")
            httpReturn += "Done !"

        elif page == "stopapp":
            print "A stop application request has arrived."
            httpReturn = "in progress..."
            os.system("sudo pkill -f Kinect")
            os.system("pkill -f firefox")
            httpReturn += "Done !"

        elif page == "kinectcalib":
            print "A calib kinect request has arrived."
            httpReturn = "in progress..."
            os.system("wget http://localhost:8080/calib")
            httpReturn = "Done !"

        elif page == "kinectdisplay":
            print "A display kinect request has arrived."
            httpReturn = "in progress..."
            os.system("wget http://localhost:8080/displ")
            httpReturn += "Done !"

        elif page == "upgradeapp":
            print "A upgrade application request has arrived."
            httpReturn = "in progress..."
            os.system("/home/e2ms/APPe2ms/update.sh > /home/e2ms/APPe2ms/update_sh.log")
            httpReturn += "Done !"

        elif page == "restartcontrolserver":
            print "A restart control server request has arrived."
            httpReturn = "in progress..."
            httpd.server_close()
            sys.stdout = saveout
            fsock.close()
            httpReturn += "Done !"
            exit()


        elif page == "help":
            print "A help request has arrived."
            httpReturn = "<a href='restart.py'>Restart</a><br>"
            httpReturn += "<a href='shutdown.py'>Shutdown</a><br>"
            httpReturn += "<a href='startfirefox.py'>Start Firefox</a><br>"
            httpReturn += "<a href='stopfirefox.py'>Stop Firefox</a><br>"
            httpReturn += "<a href='startkinect.py'>Start Kinect</a><br>"
            httpReturn += "<a href='stopkinect.py'>Stop Kinect</a><br>"
            httpReturn += "<a href='kinectcalib.py'>Calibrate Kinect</a><br>"
            httpReturn += "<a href='kinectdisplay.py'>Display Kinect</a><br>"
            httpReturn += "<a href='upgradeapp.py'>Upgrade application</a><br>"
            httpReturn += "<a href='restartcontrolserver.py'>Restart control server</a><br>"

        else:
            print "A unknown request has arrived."
            httpReturn = "A unknown request has arrived."

        return "<br>" + httpReturn + "<br><br><a href='file:///home/e2ms/APPe2ms/APP/index.html'>Return to application</a>"


if __name__ == '__main__':

    saveout = sys.stdout
    fsock = open('/home/e2ms/APPe2ms/control_server_py.log', 'w')
    sys.stdout = fsock
    sys.stderr = fsock

    # Discover the local ip address >localIp
    ip = get_ip()
    print "My IP address is " + ip

    server_address = ('', httpPort)
    httpd = HTTPServer(server_address, HTTPHandler)
    sa = httpd.socket.getsockname()
    print "Serving HTTP on", sa[0], "port", sa[1], "..."
    httpd.serve_forever()
    